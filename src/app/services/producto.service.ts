import { Injectable } from '@angular/core';
import { map } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';
import { HttpClient } from '@angular/common/http';
import { Producto } from '../models/producto';
import { GLOBAL } from './global';

@Injectable()
export class ProductoService{
  public url:string;

  constructor(
    public _http: HttpClient
  )
  {
    this.url = GLOBAL.url;
  }

  getProductos() {
    //peticion ajax
    return this._http.get(this.url + 'productos');
  }
}
