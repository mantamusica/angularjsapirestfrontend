import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html'
})
export class HomeComponent implements OnInit {

  public titulo:string;

  constructor() {
    this.titulo = 'WebApp Productos Angular';
   }

  ngOnInit() {
    console.log('Se ha cargado el componente home.component.ts');
  }

}
