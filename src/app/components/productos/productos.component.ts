import { Component, OnInit } from '@angular/core';
import { ProductoService } from '../../services/producto.service';
import { RouterModule, ActivatedRoute, Params } from '@angular/router';
import { Producto } from '../../models/producto';

@Component({
  selector: 'app-productos',
  templateUrl: './productos.component.html',
  providers: [ProductoService]
})

export class ProductosComponent implements OnInit {

  public titulo:string;
  public productos: Producto[];

  constructor(
    private _route: ActivatedRoute,
    private _router: RouterModule,
    private _productoService: ProductoService)
    {
      this.titulo = 'Listado de Productos';
    }

  ngOnInit() {
    console.log('productos.component.ts cargado.');
    console.log(this._productoService.getProductos());
    this._productoService.getProductos().subscribe(
      result => {
        if(result['code'] === 200) {
          this.productos = result['data'];
        } else {
          this.productos = result['data'];
        }

      },
      error => {
        console.log(<any>error);
      }
    )
  }

}
